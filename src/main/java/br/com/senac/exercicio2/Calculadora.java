package br.com.senac.exercicio2;

public class Calculadora {

    private final double PORCENTAGEM_DISTRIBUIDOR = 0.28;
    private final double IMPOSTOS = 0.45;
    
    public double calcular(double custo) {

        double imposto = custo * IMPOSTOS;
        double margemLucro = custo * PORCENTAGEM_DISTRIBUIDOR;
        double precoFinal = custo + imposto + margemLucro;

        return precoFinal;
    }

}
