package br.com.senac.exercicio2.test;

import br.com.senac.exercicio2.Calculadora;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculadoraTeste {

    public CalculadoraTeste() {
    }

    @Test
    public void deveCalcularFinalConsumidor() {

        Calculadora calculadora = new Calculadora();
        double valorCusto = 10000;
        double resultado = calculadora.calcular(valorCusto);
        
        assertEquals(17300, resultado, 0.01);
        
    }
    

}
